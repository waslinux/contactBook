import React, { useEffect, useState, memo } from "react";
import {
  Container,
  Dimmer,
  Loader,
  Table,
  Input,
  Icon,
} from "semantic-ui-react";
import api from "../api/serverConnect";

const ContactCard = (props) => {
  const [load, setLoad] = useState(true);
  const [search, setSearch] = useState("");
  const [data, setData] = useState([]);

  const del = async (id, name) => {
    const confirm = window.confirm(
      "Do you want to delete the contact ",
      "'",
      name,
      "'",
      " ?"
    );
    if (confirm) {
      let password = prompt("Enter the delete password!");
      if (password != 100) {
        alert("Incorrect password");
      } else {
        api
          .delete(`/contacts/${id}`, data)
          .then((data) => {
            setData(data);
          })
          .catch((error) => console.log("Error: ", console.error));
      }
    } else return;
  };

  useEffect(() => {
    setInterval(() => {
      setLoad(false);
    }, 1000);
  }, []);

  return (
    <Container>
      <div>
        <Input
          className="search-input"
          onChange={(event) => {
            setSearch(event.target.value);
          }}
          placeholder="Search..."
        />
      </div>
      <div className="table">
        <Table celled selectable>
          <Table.Header>
            <Table.Row id="teste">
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Phone</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Company</Table.HeaderCell>
              <Table.HeaderCell>Department</Table.HeaderCell>
              <Table.HeaderCell>Remove</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          {load === true ? (
            <Dimmer active>
              <Loader size="massive">Loading...</Loader>
            </Dimmer>
          ) : (
            props.contacts
              .filter((contact) => {
                if (search === "") {
                  return contact;
                } else if (
                  contact.name.toLowerCase().includes(search.toLowerCase())
                ) {
                  return contact;
                }
              })
              .sort((a, b) => (a.name > b.name ? 1 : -1))
              .map((contact, index) => (
                <Table.Body className="cell" key={index}>
                  <Table.Row className="cells" key={contact.id}>
                    <Table.Cell key={1}>{contact.name}</Table.Cell>
                    <Table.Cell key={2}>{contact.phone}</Table.Cell>
                    <Table.Cell key={4}>{contact.email}</Table.Cell>
                    <Table.Cell key={3}>{contact.company}</Table.Cell>
                    <Table.Cell key={5}>{contact.department}</Table.Cell>
                    <Table.Cell>
                      <Icon
                        name="trash"
                        id="trash"
                        onClick={() => del(contact.id, contact.name)}
                      />
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              ))
          )}
        </Table>
      </div>
    </Container>
  );
};

export default memo(ContactCard);
