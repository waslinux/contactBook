import React, { useEffect, useState } from "react";
import { Container, Button, Image } from "semantic-ui-react";
import AddContact from "../components/AddContact";
import ContactCard from "../components/ContactCard";
import logo from "../images/logo.png";
import api from "../api/serverConnect";

const ContactList = () => {
  const [openModal, setOpenModal] = useState(false);
  const [contacts, setContacts] = useState([]);

  const retrieveContacts = async () => {
    const response = await api.get("/contacts");
    return response.data;
  };
  useEffect(() => {
    const getAllContacts = async () => {
      const allContacts = await retrieveContacts();
      if (allContacts) setContacts(allContacts);
    };
    getAllContacts();
  }, [contacts]);

  return (
    <Container>
      <div>
        {openModal ? (
          <AddContact closeModal={setOpenModal} />
        ) : (
          <div className="mainContainer">
            <center>
              <div className="headerTitle">
                <Image centered size="large" src={logo} id="img" />
              </div>
            </center>
            <div className="header-top">
              <div className="header-list">
                <Button
                  color="black"
                  id="addBtn"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                >
                  Add
                </Button>
              </div>
              <ContactCard contacts={contacts} />
            </div>
          </div>
        )}
      </div>
    </Container>
  );
};

export default ContactList;
