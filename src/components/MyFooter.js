import React from "react";
import "./App.css";
const MyFooter = () => {
  return (
    <div className="footer" center>
      <h4>Developed by Wander Augusto</h4>
    </div>
  );
};
export default MyFooter;
