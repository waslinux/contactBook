import React, { useState } from "react";
import { Container, Button, Icon } from "semantic-ui-react";
import api from "../api/serverConnect";

const AddContact = ({ closeModal }) => {
  const [data, setData] = useState({
    name: "",
    phone: "",
    email: "",
    company: "",
    departament: "",
  });

  const onSubmit = async (e) => {
    if (data.name === "" || data.phone === "") {
      alert("add name and at least one phone!");
    } else {
      api.post("/contacts", data).then(() => {
        setData({
          name: "",
          phone: "",
          email: "",
          company: "",
          departament: "",
        });
      });
      alert("Contact " + "'" + data.name + "'" + " added").catch((error) =>
        console.log("Error: ", error)
      );
    }
  };

  return (
    <Container id="modal">
      <center>
        <h3>Add Contact</h3>
      </center>
      <center>
        <div className="line"></div>
      </center>
      <form className="form">
        <div>
          <div className="line-input">
            <input
              placeholder="Name"
              type="text"
              value={data.name}
              onChange={(e) => setData({ ...data, name: e.target.value })}
            />
            <input
              placeholder="Phone"
              type="text"
              value={data.phone}
              onChange={(e) => setData({ ...data, phone: e.target.value })}
            />
          </div>
          <div className="line-input">
            <input
              placeholder="Email"
              type="email"
              value={data.email}
              onChange={(e) => setData({ ...data, email: e.target.value })}
            />

            <input
              placeholder="Company"
              type="text"
              value={data.company}
              onChange={(e) => setData({ ...data, company: e.target.value })}
            />
          </div>
          <div className="line-input2">
            <input
              placeholder="Departament"
              type="text"
              value={data.departament}
              onChange={(e) =>
                setData({ ...data, departament: e.target.value })
              }
            />
          </div>
          <div>
            <Button.Group id="btn">
              <Button id="closeBtn" negative onClick={() => closeModal(false)}>
                <Icon name="cancel" id="icon" />
              </Button>
              <Button.Or text="or" />
              <Button id="saveBtn" positive type="submit" onClick={onSubmit}>
                <Icon name="checkmark" id="icon" />
              </Button>
            </Button.Group>
          </div>
        </div>
      </form>
    </Container>
  );
};

export default AddContact;
