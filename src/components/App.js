import React from "react";
import ContactList from "./ContactList";
import "semantic-ui-css/semantic.min.css";
import MyFooter from "./MyFooter";

function App() {
  return (
    <div className="main">
      <ContactList />
      <MyFooter />
    </div>
  );
}

export default App;
