<h1 align="center"> CONTACT BOOK </h1> 

## This project was developed with React, Semantic UI React and Json Server!

* To download the project follow the instructions bellow:
```bash
1. git clone https://codeberg.org/waslinux/contactBook
2. cd contactBook
```

* Install the dependencies and start the application:
```bash
3. npm install or yarn install
4. npm start
```

* start Json Server in other port:
```bash
5. npx json-server --watch db.json --port 3006
```


# ScreenShot # 

<p align="center" width="100%">
    <img src="./src/images/image.png">
</p>